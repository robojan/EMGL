#include <emgl/emgl_ui.h>

namespace emgl
{

	Point::Point(emgl_coord_t x, emgl_coord_t y) :
		x(x), y(y)
	{
	}

	Point::Point() :
		x(0), y(0)
	{
	}

	void Point::setPos(emgl_coord_t x, emgl_coord_t y)
	{
		this->x = x;
		this->y = y;
	}

	Point Point::operator-() const {
		return Point(-x, -y);
	}

	Point &Point::operator+=(const Point &other)
	{
		this->x += other.x;
		this->y += other.y;
		return *this;
	}

	Point &Point::operator-=(const Point &other)
	{
		this->x -= other.x;
		this->y -= other.y;
		return *this;
	}

	Point Point::operator+(const Point &other) const
	{
		return Point(this->x + other.x, this->y + other.y);
	}

	Point Point::operator-(const Point &other) const
	{
		return Point(this->x - other.x, this->y - other.y);
	}

	Size::Size(emgl_length_t width, emgl_length_t height) :
		width(width), height(height)
	{
	}

	void Size::setSize(emgl_length_t width, emgl_length_t height)
	{
		this->width = width;
		this->height = height;
	}

	Size Size::operator-() const {
		return Size(-width, -height);
	}
	
	Size &Size::operator+=(const Size &other)
	{
		this->width += other.width;
		this->height += other.height;
		return *this;
	}

	Size &Size::operator-=(const Size &other)
	{
		this->width -= other.width;
		this->height -= other.height;
		return *this;
	}

	Size Size::operator+(const Size &other) const
	{
		return Size(this->width + other.width, this->height + other.height);
	}

	Size Size::operator-(const Size &other) const
	{
		return Size(this->width - other.width, this->height - other.height);
	}

	Rectangle::Rectangle(emgl_coord_t x, emgl_coord_t y, emgl_length_t width, emgl_length_t height) :
		x(x), y(y), width(width), height(height)
	{
	}

	Rectangle::Rectangle(Point pos, Size size) :
		x(pos.x), y(pos.y), width(size.width), height(size.height)
	{

	}

	Rectangle Rectangle::operator+(const Point &other) const
	{
		return Rectangle(this->x + other.x, this->y + other.y, this->width, this->height);
	}

	Rectangle Rectangle::operator-(const Point &other) const
	{
		return Rectangle(this->x - other.x, this->y - other.y, this->width, this->height);
	}
	
	Size Rectangle::getSize() const
	{
		return Size(width, height);
	}

	Point Rectangle::getPos() const
	{
		return Point(x, y);
	}

	void Rectangle::inset(int offset) {
		x += offset;
		y += offset;
		if (width >= 2*offset) width -= 2*offset;
		if (height >= 2 * offset) height -= 2 * offset;
	}

	bool Rectangle::IsInside(const Point &pos) const
	{
		return pos.x > x && pos.y > y && pos.x < x + width && pos.y < y + height;
	}

}