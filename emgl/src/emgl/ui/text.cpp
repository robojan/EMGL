#include <emgl/emgl_ui.h>
#include <emgl/debug.h>
#include <emgl/font.h>

#include <cstring>

extern const EMGL_font_t *EMGL_DEFAULT_FONT;

namespace emgl {
	
	TextLayout::TextLayout() : _boundingBox(0, 0, 0, 0)
	{

	}

	void TextLayout::reset()
	{
		_layout.clear();
	}

	int TextLayout::getNumberOfGlyphs(const char *str) const
	{
		int result = 0;
		while(*str != '\0') {
			if(*str != ' ' && *str != '\t' && *str != '\n') {
				result++;
			}
			str++;
		}
		return result;
	}

	void TextLayout::doLayout(const char *str, const EMGL_font_t *font, Size size /*= Size(-1, -1) */,
		Alignment hAlignment /*= ALIGN_LEFT*/, Alignment vAlignment /*= ALIGN_TOP*/, 
		bool wrap /*= false*/)
	{
		emgl_coord_t x = 0;
		emgl_coord_t y = -font->ascender;
		std::set<int> linebreaks;
		_boundingBox = Rectangle(0, 0, 0, 0);
		_layout.clear();
		_layout.reserve(getNumberOfGlyphs(str));

		const char *ptr = str;
		while (*ptr != '\0') {
			if (*ptr == '\n') {
				x = 0;
				y -= font->ascender;
				if (_layout.size() > 0) {
					linebreaks.insert(_layout.size() - 1);
				}
			}
			else if (*ptr == '\t') {
				int remainder = x % EMGL_TAB_SIZE;
				x += remainder == 0 ? 0 : (EMGL_TAB_SIZE - remainder);
			}
			else {
				const EMGL_glyph_t *glyph = emgl_getGlyph(font, *ptr);
				if (glyph) {
					if (x + glyph->bitmapWidth > size.width && size.width > 0 && wrap) {
						x = 0;
						y -= font->ascender;
						linebreaks.insert(_layout.size()-1);
					}
					if (glyph->bitmapWidth > 0 && glyph->bitmapHeight > 0) {
						Point glyphPos(x + glyph->bearingX, y + glyph->bearingY - glyph->bitmapHeight);
						emgl_coord_t left = glyphPos.x;
						if (_boundingBox.x > left) _boundingBox.x = left;
						emgl_coord_t width = left + glyph->bitmapWidth - _boundingBox.x;
						if (_boundingBox.width < width) _boundingBox.width = width;
						emgl_coord_t top = glyphPos.y + glyph->bitmapHeight;
						emgl_coord_t bottom = glyphPos.y;
						emgl_coord_t height = _boundingBox.y + _boundingBox.height - bottom;
						if (_boundingBox.height < height) _boundingBox.height = height;
						if (_boundingBox.y > bottom) _boundingBox.y = bottom;
						_layout.push_back(GlyphPos(*ptr, glyphPos, Size(glyph->bitmapWidth, glyph->bitmapHeight)));
					}
					x += glyph->advanceX;
					y -= glyph->advanceY;
				}
			}
			ptr++;
		}
		if(_layout.size() > 0) {
			linebreaks.insert(_layout.size()-1);
			if (size.width > 0) {
				doAlign(hAlignment, size.width, linebreaks);
			}
		}
		doVerticalShift(vAlignment, size.height);
		createFillings(size);
	}

	void TextLayout::createFillings(Size size)
	{
		_fillings.clear();
		if (size.width < 0) size.width = _boundingBox.width;
		if (size.height < 0) size.height = _boundingBox.height;
		emgl_coord_t left = _boundingBox.x;
		emgl_coord_t bottom = _boundingBox.y;
		emgl_coord_t right = _boundingBox.x + _boundingBox.width;
		emgl_coord_t top = _boundingBox.y + _boundingBox.height;

		if (left > 0) {
			_fillings.push_back(Rectangle(0, 0, left, size.height));
		}
		if (size.width - right > 0) {
			_fillings.push_back(Rectangle(right, 0, size.width - right, 
				size.height));
		}
		if (bottom > 0) {
			_fillings.push_back(Rectangle(left, 0, right - left, bottom));
		}
		if (size.height - top > 0) {
			_fillings.push_back(Rectangle(left, top, right - left, size.height - top));
		}
		for (emgl_coord_t x = left; x < right; x++) {
			std::vector<GlyphPos>::const_iterator it = _layout.begin();
			emgl_coord_t y = top-1;
			while (it != _layout.end() && y > bottom)
			{
				if(x >= it->pos.x && x < it->pos.x + it->size.width) {
					emgl_coord_t yend = it->pos.y + it->size.height;
					if (y - yend > 0) {
						addFilling(x, y, yend);
					}
					y = it->pos.y - 1;
				}
				it++;
			}
			if(y > bottom) {
				addFilling(x, y, bottom);
			}
		}
	}

	void TextLayout::addFilling(emgl_coord_t x, emgl_coord_t top, emgl_coord_t bottom) {
		emgl_length_t height = top - bottom + 1;
		for(std::vector<Rectangle>::iterator it = _fillings.begin(), end = _fillings.end();
			it != end; it++) {
			if(it->x+it->width == x && it->y == bottom && it->height == height) {
				it->width++;
				return;
			}
		}
		_fillings.push_back(Rectangle(x, bottom, 1, height));	
	}

	void TextLayout::reduceFillings(std::list<Rectangle> &fillings)
	{
		std::list<Rectangle>::iterator it1, it2;
		for (it1 = fillings.begin(); it1 != fillings.end(); it1++)
		{
			for (it2 = it1; it2 != fillings.end(); )
			{
				if (it1->x + it1->width == it2->x && it1->y == it2->y && it1->height == it2->height) {
					it1->width++;
					fillings.erase(it2++);
				}
				else {
					it2++;
				}
			}
		}
	}

	void TextLayout::doAlign(Alignment hAlignment, emgl_length_t width, const std::set<int> &linebreaks) {
		EMGL_ASSERT("linebreaks must have at least 1 element!", linebreaks.size() > 0);
		EMGL_ASSERT("width must be > 0!", width > 0);
		int lineWidth;
		int lineStart = 0;
		int lineEnd = 0;
		int baseOffset = _boundingBox.x < 0 ? -_boundingBox.x : 0;
		_boundingBox.x += baseOffset;
		_boundingBox.width += baseOffset;
		switch (hAlignment) {
		case ALIGN_CENTER:
			_boundingBox.x += (width - _boundingBox.width) / 2;
			break;
		case ALIGN_RIGHT:
			_boundingBox.x += width - _boundingBox.width;
		}
		std::set<int>::iterator it = linebreaks.begin();
		do {
			lineEnd = *it;
			const GlyphPos &lastGlyph = _layout.at(lineEnd);
			lineWidth = lastGlyph.pos.x + lastGlyph.size.width + 2 ;
			emgl_coord_t offset;
			switch(hAlignment) {
			default:
			case ALIGN_LEFT:
				offset = baseOffset;
				break;
			case ALIGN_CENTER:
				offset = baseOffset + (width - lineWidth) / 2;
				break;
			case ALIGN_RIGHT:
				offset = baseOffset + width - lineWidth;
				break;
			}
			for (int i = lineStart; i <= lineEnd; i++) {
				_layout[i].pos.x += offset;
			}
			lineStart = lineEnd+1;
			it++;
		} while (it != linebreaks.end());
	}

	void TextLayout::doVerticalShift(Alignment vAlign, emgl_length_t height)
	{
		if (_layout.size() == 0) {
			return;
		}
		emgl_length_t size = _boundingBox.height;
		emgl_coord_t offset = size;
		if (height > 0)
		{
			switch (vAlign) {
			default:
			case ALIGN_TOP:
				offset = height;
				break;
			case ALIGN_CENTER:
				offset = size + (height - size) / 2;
				break;
			case ALIGN_BOTTOM:
				offset = size;
				break;
			}
		}
		_boundingBox.y += offset;
		for (std::vector<GlyphPos>::iterator glyph = _layout.begin();
			glyph != _layout.end(); glyph++) {
			glyph->pos.y += offset;
		}
	}

	const std::vector<Rectangle> &TextLayout::getFillingsList() const
	{
		return _fillings;
	}

	Size TextLayout::getSize() const
	{
		return _boundingBox.getSize();
	}

	std::vector<TextLayout::GlyphPos>::iterator TextLayout::begin() {
		return _layout.begin();
	}

	std::vector<TextLayout::GlyphPos>::iterator TextLayout::end() {
		return _layout.end();
	}

	std::vector<TextLayout::GlyphPos>::const_iterator TextLayout::cbegin() const {
		return _layout.begin();
	}

	std::vector<TextLayout::GlyphPos>::const_iterator TextLayout::cend() const {
		return _layout.end();
	}

	Text::Text(Element *parent, Point pos, Size size, const char *text, int id, Alignment hAlignment,
		Alignment vAlignment, bool wrap) :
		Element(parent, pos, size, id), _backgroundColor(EMGL_DEFAULT_BACKGROUND_COLOR),
		_foregroundColor(EMGL_DEFAULT_FOREGROUND_COLOR), _font(EMGL_DEFAULT_FONT),
		_text(NULL), _hAlignment(hAlignment), _vAlignment(vAlignment), _wrap(wrap)
	{
		EMGL_ASSERT("text != nullptr", text != NULL);
		int textLen = strlen(text);
		_text = new char[textLen + 1];
		memcpy(_text, text, textLen + 1);
		calculateLayout();
		Size layoutSize = _layout.getSize();
		if (size.height < 0) size.height = layoutSize.height;
		if (size.width < 0) size.width = layoutSize.width;
		SetSize(size);
	}

	Text::~Text()
	{
		if (_text) {
			delete[] _text;
			_text = NULL;
		}
	}

	emgl::Point Text::getGlyphPos(int idx)
	{
		std::vector<TextLayout::GlyphPos>::const_iterator glyph = _layout.cbegin();
		while(idx > 0) {
			if(glyph == _layout.cend()) {
				break;
			}
			glyph++;
			idx--;
		}
		if(glyph == _layout.cend()) {
			return GetPos();
		}
		return glyph->pos;
	}

	void Text::setForeground(emgl_color_t color)
	{
		_foregroundColor = color;
	}

	void Text::setBackground(emgl_color_t color)
	{
		_backgroundColor = color;
	}

	void Text::setFont(const EMGL_font_t *font)
	{
		_font = font;
		calculateLayout();
	}

	emgl_color_t Text::getForeground() const
	{
		return _foregroundColor;
	}

	emgl_color_t Text::getBackground() const
	{
		return _backgroundColor;
	}

	const EMGL_font_t *Text::getFont() const
	{
		return _font;
	}

	const char *Text::getText() const
	{
		return _text;
	}

	void Text::setText(const char *text)
	{
		EMGL_ASSERT("text != nullptr", text != NULL);
		int textLen = strlen(text);
		delete[] _text;
		_text = new char[textLen + 1];
		memcpy(_text, text, textLen + 1);
		calculateLayout();
	}

	void Text::calculateLayout()
	{
		_layout.reset();
		_layout.doLayout(_text, _font, GetSize(), _hAlignment, _vAlignment, _wrap);
	}

	void emgl::Text::Draw(PaintContext &PC, Rectangle &area)
	{
		const std::vector<Rectangle> &fillings = _layout.getFillingsList();

		for (std::vector<Rectangle>::const_iterator it = fillings.begin(), end = fillings.end();
			it != end; it++) {
			PC.drawFilledRectangle(*it, _backgroundColor);
		}

		PC.drawText(_font, _layout, _foregroundColor, _backgroundColor);
	}

}