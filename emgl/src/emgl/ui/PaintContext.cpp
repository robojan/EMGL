#include <emgl/emgl_ui.h>
#include <algorithm>
#include <vector>

namespace emgl
{

	PaintContext::PaintContext(Point origin, Size size) :
		_origin(origin), _elementSize(size)
	{
	}

	PaintContext::PaintContext(Rectangle area) :
		_origin(area.getPos()), _elementSize(area.getSize())
	{
	}

	PaintContext::~PaintContext()
	{
	}

	void PaintContext::offsetOrigin(Point offset)
	{
		_origin += offset;
	}

	void PaintContext::setOrigin(Point origin)
	{
		_origin = origin;
	}

	void PaintContext::setSize(Size size)
	{
		_elementSize = size;
	}

	void PaintContext::drawFilledRectangle(const Rectangle & rect, emgl_color_t color)
	{
		emgl_coord_t zero = 0;
		emgl_coord_t x = std::max(rect.x, zero);
		emgl_coord_t y = std::max(rect.y, zero);
		emgl_coord_t r = rect.x + rect.width;
		emgl_coord_t t = rect.y + rect.height;
		emgl_coord_t w1 = std::min(r, _elementSize.width) - x;
		emgl_coord_t h1 = std::min(t, _elementSize.height) - y;
		emgl_coord_t width = std::max(w1, zero);
		emgl_coord_t height = std::max(h1, zero);

		emgl_drawFilledRectangle(_origin.x + x, _origin.y + y, width, height, color);
	}

	void PaintContext::drawText(const EMGL_font_t *font, const TextLayout &layout, 
		emgl_color_t foreColor, emgl_color_t backColor)
	{
		emgl_coord_t lastX = 0;
		for (std::vector<TextLayout::GlyphPos>::const_iterator glyph = layout.cbegin(); 
			glyph != layout.cend(); glyph++) {
			emgl_coord_t x = _origin.x + glyph->pos.x;
			emgl_coord_t y = _origin.y + glyph->pos.y;
			if (x < _origin.x || y < _origin.y) continue;
			if (glyph->pos.x + glyph->size.width > _elementSize.width ||
				glyph->pos.y + glyph->size.height > _elementSize.height) continue;
			emgl_drawGlyphWithoutBearings(font, glyph->glyph, &x, &y, foreColor, backColor);
		}
	}

	void PaintContext::drawRoundedRectangle(const Rectangle &rect, int radius, emgl_color_t color)
	{
		emgl_coord_t zero = 0;
		emgl_coord_t x = std::max(rect.x, zero);
		emgl_coord_t y = std::max(rect.y, zero);
		emgl_coord_t width = std::max((emgl_coord_t)(std::min((emgl_coord_t)(rect.x + rect.width), _elementSize.width) - x), zero);
		emgl_coord_t height = std::max((emgl_coord_t)(std::min((emgl_coord_t)(rect.y + rect.height), _elementSize.height) - y), zero);

		emgl_drawRoundedRectangle(_origin.x + x, _origin.y + y, width, height, radius, color);
	}

	void PaintContext::drawFilledRoundedRectangle(const Rectangle &rect, int radius, emgl_color_t color)
	{
		emgl_coord_t zero = 0;
		emgl_coord_t x = std::max(rect.x, zero);
		emgl_coord_t y = std::max(rect.y, zero);
		emgl_coord_t width = std::max((emgl_coord_t)(std::min((emgl_coord_t)(rect.x + rect.width), _elementSize.width) - x), zero);
		emgl_coord_t height = std::max((emgl_coord_t)(std::min((emgl_coord_t)(rect.y + rect.height), _elementSize.height) - y), zero);

		emgl_drawFilledRoundedRectangle(_origin.x + x, _origin.y + y, width, height, radius, color);
	}

	void PaintContext::drawRectangleBezelRaisedFilled(const Rectangle &rect, emgl_color_t color)
	{
		Point bl = rect.getPos();
		Point tl = bl;
		tl.y += rect.height - 1;
		Point tr = tl;
		tr.x += rect.width - 1;
		Point br = bl;
		br.x += rect.width - 1;
		Point dx(1, 0);
		Point dy(0, 1);
		drawLine(bl, tl, emgl_colorLighter(color));
		drawLine(tl + dx, tr, emgl_colorLighter(color));
		drawLine(bl + dx, br, emgl_colorDarker(color));
		drawLine(tr - dy, br + dy, emgl_colorDarker(color));
		drawFilledRectangle(Rectangle(bl.x + 1, bl.y + 1, rect.width - 2, rect.height - 2), color);
	}

	void PaintContext::drawRectangleBezelLoweredFilled(const Rectangle &rect, emgl_color_t color)
	{
		Point bl = rect.getPos();
		Point tl = bl;
		tl.y += rect.height - 1;
		Point tr = tl;
		tr.x += rect.width - 1;
		Point br = bl;
		br.x += rect.width - 1;
		Point dx(1, 0);
		Point dy(0, 1);
		drawLine(bl + dy, tl, emgl_colorDarker(color));
		drawLine(tl + dx, tr - dx, emgl_colorDarker(color));
		drawLine(bl, br, emgl_colorLighter(color));
		drawLine(br + dy, tr, emgl_colorLighter(color));
		drawFilledRectangle(Rectangle(bl.x + 1, bl.y + 1, rect.width - 2, rect.height - 2), color);
	}

	void PaintContext::drawFilledPolygon(const emgl_coord_t *checkXValues, const emgl_coord_t *checkYValues, int count, emgl::Point pos, emgl_color_t color)
	{
		std::vector<emgl_coord_t> xCoords;
		std::vector<emgl_coord_t> yCoords;
		xCoords.reserve(count);
		yCoords.reserve(count);
		for(int i = 0; i < count; i++)
		{
			xCoords.push_back(checkXValues[i] + pos.x + _origin.x);
			yCoords.push_back(checkYValues[i] + pos.y + _origin.y);
		}
		emgl_drawFilledPolygon(&xCoords[0], &yCoords[0], count, color);
	}

	void PaintContext::drawLine(const Point &p1, const Point &p2, emgl_color_t color)
	{
		emgl_drawLine(_origin.x + p1.x, _origin.y + p1.y, _origin.x + p2.x, _origin.y + p2.y, color);
	}

	void PaintContext::drawCircle(const Point &center, int radius, emgl_color_t color)
	{
		emgl_drawCircle(_origin.x + center.x, _origin.y + center.y, radius, color);
	}
	
	void PaintContext::setPixel(const Point &p, emgl_color_t color)
	{
		emgl_setPixel(_origin.x + p.x, _origin.y + p.y, color);
	}

}