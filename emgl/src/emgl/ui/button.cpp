#include <emgl/emgl_ui.h>

namespace emgl {

	Button::Button(Element *parent, Point pos, Size size, const char *label, int id) :
		Panel(parent, pos, size, id), _pressed(false)
	{
		_label = new Text(this, Point(1, 1), size-Size(2,2), label, -1, ALIGN_CENTER, ALIGN_CENTER, true);
	}

	Button::~Button()
	{

	}

	void Button::setFont(const EMGL_font_t *font)
	{
		_label->setFont(font);
	}

	const EMGL_font_t * Button::getFont() const
	{
		return _label->getFont();
	}

	void Button::setForeground(emgl_color_t color)
	{
		Panel::setForeground(color);
		_label->setForeground(color);
	}
	
	void Button::setBackground(emgl_color_t color)
	{
		Panel::setBackground(color);
		_label->setBackground(color);
	}
	

	void Button::Draw(PaintContext &PC, Rectangle &area)
	{
		Rectangle borderRect(Point(0, 0), GetSize());
		if (_pressed) {
			PC.drawRectangleBezelLoweredFilled(borderRect, getBackground());
		}
		else {
			PC.drawRectangleBezelRaisedFilled(borderRect, getBackground());
		}
	}

	void Button::setLabel(const char *label)
	{
		_label->setText(label);
	}

	bool Button::IsPressed() const {
		return _pressed;
	}

	void Button::SetPressed(bool pressed, bool event) {
		_pressed = pressed;
		if(event && _onClickCallbackFunc) {
			_onClickCallbackFunc(this, _userValue, pressed);
		}
		Refresh();
	}

	bool Button::OnPressedEvent(Point pos, bool pressed) {
		if (!GetClientRect().IsInside(pos)) return false;

		if(_onClickCallbackFunc && !_onClickCallbackFunc(this, _userValue, pressed))
			return false;

		SetPressed(pressed);

		return true;
	}

	FlatButton::FlatButton(Element *parent, Point pos, Size size, const char *label, int id) :
		Button(parent, pos, size, label, id)
	{

	}

	FlatButton::~FlatButton() 
	{

	}

	void FlatButton::Draw(PaintContext &PC, Rectangle &area) {
		Rectangle rect(Point(0, 0), GetSize());
		emgl_color_t color;
		if (_pressed) {
			color = emgl_colorLighter(getBackground());
		}
		else {
			color = emgl_colorDarker(getBackground());
		}
		_label->setBackground(color);
		PC.drawFilledRectangle(rect, color);
	}
}
