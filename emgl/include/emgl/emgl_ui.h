/*
 * emgl_ui.h
 *
 * Created: 25-2-2017 21:13:21
 *  Author: Robojan
 */ 


#ifndef EMGL_UI_H_
#define EMGL_UI_H_

#include <emgl/emgl.h>
#include <set>
#include <vector>
#include <list>

namespace emgl {

	enum Alignment {
		ALIGN_LEFT,
		ALIGN_CENTER,
		ALIGN_RIGHT,
		ALIGN_TOP = ALIGN_LEFT,
		ALIGN_BOTTOM = ALIGN_RIGHT,
	};

	class Point {
	public:
		emgl_coord_t x;
		emgl_coord_t y;

		Point(emgl_coord_t x, emgl_coord_t y);
		Point();

		void setPos(emgl_coord_t x, emgl_coord_t y);

		Point operator-() const;
		Point &operator+=(const Point &other);
		Point &operator-=(const Point &other);
		Point operator+(const Point &other) const;
		Point operator-(const Point &other) const;
	};

	class Size {
	public:
		emgl_length_t width;
		emgl_length_t height;

		Size(emgl_length_t width, emgl_length_t height);
		
		void setSize(emgl_length_t width, emgl_length_t height);

		Size operator-() const;
		Size &operator+=(const Size &other);
		Size &operator-=(const Size &other);
		Size operator+(const Size &other) const;
		Size operator-(const Size &other) const;
	};

	class Rectangle {
	public:
		emgl_coord_t x;
		emgl_coord_t y;
		emgl_length_t width;
		emgl_length_t height;

		Rectangle(emgl_coord_t x, emgl_coord_t y, emgl_length_t width, emgl_length_t height);
		Rectangle(Point pos, Size size);

		Size getSize() const;
		Point getPos() const;

		void inset(int offset);

		bool IsInside(const Point &pos) const;

		Rectangle operator+(const Point &other) const;
		Rectangle operator-(const Point &other) const;
	};

	class TextLayout {
	public:
		class GlyphPos {
		public:
			GlyphPos(emgl_U32 glyph, Point point, Size size) :
				glyph(glyph), pos(point), size(size) {}
			emgl_U32 glyph;
			Point pos;
			Size size;
		};
		TextLayout();

		void reset();
		void doLayout(const char *str, const EMGL_font_t *font, Size size = Size(-1,-1), 
			Alignment hAlignment = ALIGN_LEFT, Alignment vAlignment = ALIGN_BOTTOM, bool wrap = false);
		Size getSize() const;

		std::vector<GlyphPos>::iterator begin();
		std::vector<GlyphPos>::iterator end();
		std::vector<GlyphPos>::const_iterator cbegin() const;
		std::vector<GlyphPos>::const_iterator cend() const;

		const std::vector<Rectangle> &getFillingsList() const;

	private:
		void doAlign(Alignment hAlignment, emgl_length_t width, const std::set<int> &linebreaks);
		void doVerticalShift(Alignment vAlignment, emgl_length_t height);
		void createFillings(Size size);
		void reduceFillings(std::list<Rectangle> &fillings);
		int getNumberOfGlyphs(const char *str) const;
		void addFilling(emgl_coord_t x, emgl_coord_t y, emgl_coord_t y2);

		std::vector<GlyphPos> _layout;
		std::vector<Rectangle> _fillings;
		Rectangle _boundingBox;
	};

	class PaintContext {
	public:
		PaintContext(Point origin, Size size);
		PaintContext(Rectangle area);
		~PaintContext();

		void offsetOrigin(Point offset);
		void setOrigin(Point origin);
		void setSize(Size size);

		void drawFilledRectangle(const Rectangle &rect, emgl_color_t color);
		void drawText(const EMGL_font_t *font, const TextLayout &layout, emgl_color_t foreColor, 
			emgl_color_t backColor);
		void drawRoundedRectangle(const Rectangle &rect, int radius, emgl_color_t color);
		void drawFilledRoundedRectangle(const Rectangle &rect, int radius, emgl_color_t color);
		void drawRectangleBezelRaisedFilled(const Rectangle &rect, emgl_color_t color);
		void drawRectangleBezelLoweredFilled(const Rectangle &rect, emgl_color_t color);
		void drawLine(const Point &p1, const Point &p2, emgl_color_t color);
		void drawCircle(const Point &center, int radius, emgl_color_t color);
		void setPixel(const Point &p, emgl_color_t color);
		void drawFilledPolygon(const emgl_coord_t *checkXValues, const emgl_coord_t *checkYValues, int count, emgl::Point pos, emgl_color_t color);
	private:
		Point _origin;
		Size _elementSize;
	};

	class Element {
	public:
		Element(Element *parent, Point pos, Size size, int ID = -1);
		virtual ~Element();

		void AddChild(Element *child, bool own = true);
		void RemoveChild(Element *child);
		virtual void Refresh();

		virtual void SetPos(Point pos);
		virtual void SetSize(Size size);

		virtual Point GetPos() const;
		Point GetScreenPos() const;
		virtual Size GetSize() const;
		virtual Rectangle GetRect() const;
		virtual Rectangle GetClientRect() const;
		virtual int GetID() const;

		bool ProcessPressedEvent(Point pos, bool pressed);

		void setOnClickCallback(bool (*onClickCallbackFunc)(Element *, void *, bool), void *userValue);

	protected:
		virtual void Draw(PaintContext &PC, Rectangle &area);
		void Refresh(PaintContext & PC, Rectangle & area);

		void removeChildren();
		void setParent(Element *parent);

		virtual bool OnPressedEvent(Point pos, bool pressed);

		bool (*_onClickCallbackFunc)(Element *sender, void *user, bool pressed);
		void *_userValue;
	private:
		struct childinfo {
			Element *child;
			bool own;
		};
		int _id;
		Element *_parent;
		std::list<struct childinfo> _children;
		Point _pos;
		Size _size;
	};

	class Panel : public Element {
	public:
		Panel(Element *parent, Point pos, Size size, int ID = -1);
		virtual ~Panel();

		virtual void setForeground(emgl_color_t color);
		virtual void setBackground(emgl_color_t color);
		virtual emgl_color_t getForeground() const;
		virtual emgl_color_t getBackground() const;

		virtual void Draw(PaintContext &PC, Rectangle &area);
	private:
		emgl_color_t _foregroundColor;
		emgl_color_t _backgroundColor;
	};


	class Text : public Element {
	public:
		Text(Element *parent, Point pos, Size size, const char *text, int ID = -1, Alignment hAlignment = ALIGN_LEFT, 
			Alignment vAlignment = ALIGN_TOP, bool wrap = false);
		virtual ~Text();

		virtual void setForeground(emgl_color_t color);
		virtual void setBackground(emgl_color_t color);
		virtual void setFont(const EMGL_font_t *font);
		virtual emgl_color_t getForeground() const;
		virtual emgl_color_t getBackground() const;
		virtual const EMGL_font_t *getFont() const;

		virtual const char *getText() const;
		virtual void setText(const char *text);

		virtual emgl::Point getGlyphPos(int idx);

		virtual void Draw(PaintContext &PC, Rectangle &area);
	protected:
		void calculateLayout();
		char *_text;

	private:
		TextLayout _layout;
		emgl_color_t _foregroundColor;
		emgl_color_t _backgroundColor;
		const EMGL_font_t *_font;
		Alignment _hAlignment;
		Alignment _vAlignment;
		bool _wrap; 
	};

	class Button : public Panel {
	public:
		Button(Element *parent, Point pos, Size size, const char *label, int ID = -1);
		virtual ~Button();

		virtual void setForeground(emgl_color_t color);
		virtual void setBackground(emgl_color_t color);
		virtual void setFont(const EMGL_font_t *font);
		virtual const EMGL_font_t *getFont() const;

		virtual void setLabel(const char *label);

		virtual void Draw(PaintContext &PC, Rectangle &area);

		virtual bool IsPressed() const;
		virtual void SetPressed(bool pressed, bool event=false);

		virtual bool OnPressedEvent(Point pos, bool pressed);
	protected:
		Text *_label;
		bool _pressed;
	private:
	};

	class FlatButton : public Button {
	public:
		FlatButton(Element *parent, Point pos, Size size, const char *label, int ID = -1);
		virtual ~FlatButton();

		virtual void Draw(PaintContext &PC, Rectangle &area);
	};
}


#endif /* EMGL_UI_H_ */